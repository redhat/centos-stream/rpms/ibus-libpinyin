#!/bin/bash

set -x

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

cd $1

VERSION=`rpmspec -q --srpm --qf "%{version}" ibus-libpinyin.spec 2>/dev/null`

if test -d ibus-libpinyin-$VERSION-build;
then cd ibus-libpinyin-$VERSION-build;
fi

cd ibus-libpinyin-$VERSION

./configure --prefix=/usr
check_return_value $?
make check
exit $?
